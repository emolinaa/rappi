(function() {
  'use strict';

  angular
    .module('rappi')
    .directive('news',['newsService', news] );

  /** @ngInject */
  function news(newsService) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/news/news.html',
      link: function (scope, elem, attrs, ctrl) {
        getNews();
        function getNews() {
          newsService.getNews().then(function (data) {
              scope.news = data;
            }, function (error) {

            });
        };
      }
    };
    return directive;
  }
})();
