(function() {
  'use strict';

  angular
      .module('rappi')
      .service('newsService', ['$q', '$http', 'basepath', newsService]);

  /** @ngInject */
  function newsService($q, $http, basepath) {
    
    var news = null;

    this.getNews = getNews;

    function getNews() {
      var deferred = $q.defer()
      if(news) {
        deferred.resolve(news);
      } else {
        $http.get(basepath).then(function (response) {
          deferred.resolve(response.data);
        }, function () {
          deferred.reject();
        });
        return deferred.promise;
      }
    };
  }
})();
