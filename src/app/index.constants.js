/* global malarkey:false, moment:false */
(function() {
  'use strict';
  var basepath = 'data/news_mock.json';
  angular
    .module('rappi')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('basepath', basepath);

})();
