(function() {
  'use strict';

  angular
    .module('rappi', ['ngAnimate', 'ngMessages', 'ngAria', 'ngRoute', 'ui.bootstrap', 'toastr']);

})();
